;;; monitors.el --- Functions to find displays with xrandr -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2022 Jake Shilling
;;
;; Author: Jake Shilling <shilling.jake@gmail.com>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: April 11, 2022
;; Modified: April 11, 2022
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (dash))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; Provides functions which use xrandr to list connected displays
;;
;;; Code:

(require 'cl-lib)
(require 'dash)

(cl-defstruct monitors-monitor
  (name nil :readonly t)
  (connected nil :readonly t)
  (primary nil :readonly t)
  (left-offset nil :readonly t)
  (top-offset nil :readonly t)
  (width nil :readonly t)
  (height nil :readonly t)
  (left-index nil)
  (top-index nil))

(defun monitors--process-display (line)
  (let ((name (cl-first (split-string line)))
        (connected (not (string-match-p "disconnected" line)))
        (primary (string-match-p "primary" line))
        (left-offset)
        (top-offset)
        (width)
        (height))
    (when-let ((beg (string-match "[0-9]+x[0-9]+\\+[0-9]+\\+[0-9]+" line)))
      (let* ((size-and-location (substring line beg (match-end 0)))
             (els (split-string size-and-location "+"))
             (size-els (split-string (cl-first els) "x")))
        (setq width (cl-parse-integer (cl-first size-els))
              height (cl-parse-integer (cl-second size-els))
              left-offset (cl-parse-integer (cl-second els))
              top-offset (cl-parse-integer (cl-third els)))))
    (make-monitors-monitor
     :name name
     :connected connected
     :primary primary
     :left-offset left-offset
     :top-offset top-offset
     :width width
     :height height)))

(defun monitors--process-line (line acc)
  (if (string-match-p "connected" line)
      (cons (monitors--process-display line) acc)
    acc))

(defun monitors--current-buffer-lines (&optional acc)
  (let* ((beg (line-beginning-position))
         (end (line-end-position))
         (line (buffer-substring-no-properties beg end))
         (ret (monitors--process-line line acc)))
    (if (< end (point-max))
        (progn
          (goto-char (+ 1 end))
          (monitors--current-buffer-lines ret))
      ret)))

(defun monitors--get-xrandr-output ()
  (when-let ((xrandr (executable-find "xrandr")))
    (with-temp-buffer
      (process-file xrandr nil t nil)
      (goto-char (point-min))
      (monitors--current-buffer-lines))))

;;;###autoload
(defun monitors-get-connected ()
  (let* ((monitors (-filter #'monitors-monitor-connected (monitors--get-xrandr-output)))
         (left-offsets (-map #'monitors-monitor-left-offset monitors))
         (top-offsets (-map #'monitors-monitor-top-offset monitors)))
    (--each monitors
      (setf (monitors-monitor-left-index it)
            (-elem-index (monitors-monitor-left-offset it) left-offsets)
            (monitors-monitor-top-index it)
            (-elem-index (monitors-monitor-top-offset it) top-offsets)))
    (-sort (lambda (a b)
             (let ((top-a (monitors-monitor-top-index a))
                   (top-b (monitors-monitor-top-index b))
                   (left-a (monitors-monitor-left-index a))
                   (left-b (monitors-monitor-left-index b)))
               (if (= top-a top-b)
                   (< top-a top-b)
                 (< left-a left-b))))
           monitors)))

(provide 'monitors)
;;; monitors.el ends here
